package libUtils

import (
	"github.com/bwmarrin/snowflake"
)

var snowflakeNode *snowflake.Node


func init(){
	snowflakeNode, _ = snowflake.NewNode(1)
}

//雪花id
func SnowflakeId() int64 {
	return snowflakeNode.Generate().Int64()
}

//雪花id字符串型
func SnowflakeIdStr() string {	
	return snowflakeNode.Generate().String()
}