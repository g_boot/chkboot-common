/*
 * @Descripttion: 前端接口公共参数
 * @version: 1.0
 * @Author: lfzxs@qq.com
 * @Date: 2023-11-08 10:33:02
 * @LastEditors: lfzxs@qq.com
 * @LastEditTime: 2023-12-01 09:25:20
 */
package common

// PageReq 公共请求参数
type PageReq struct {
	PageNum  int     `p:"pageNum" dc:"当前页码" d:"1" db_order:"{'field':'current'}"`  
	PageSize int     `p:"pageSize" dc:"每页条数" d:"10" db_order:"{'field':'size'}"` 
	Column   string  `p:"column" dc:"排序字段"` 
	Order    string  `p:"order"  dc:"排序方式:desc,asc"` 
}

type Author struct {
	Authorization string `p:"Authorization" in:"header" dc:"Bearer {{token}}"`
}
