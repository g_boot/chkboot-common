/*
 * @Descripttion:
 * @version:
 * @Author: lfzxs@qq.com
 * @Date: 2023-11-09 08:55:35
 * @LastEditors: lfzxs@qq.com
 * @LastEditTime: 2023-12-01 17:12:06
 */
package libUtils_test

import (
	"fmt"
	"testing"

	"gitee.com/g_boot/chkboot-common/api/v1/common"
	"gitee.com/g_boot/chkboot-common/library/libUtils"
	"github.com/gogf/gf/v2/frame/g"
)

type TagTypSearchReq struct {
	g.Meta `path:"/type/list" tags:"标签管理" method:"get" summary:"标签类型查找"`
	Tenant string `p:"tenant" dc:"租户"`
	Name   string `p:"name" dc:"名称" db:"{'field':'typ_name','search':'like'}"`
	Code   string `p:"code" dc:"编码" db:"{'field':'typ_code','search':'like'}"`
	Status string `p:"status"  dc:"状态" db:"{'field':'record_status'}"`
	common.PageReq
}

func TestAdd(t *testing.T) {
	var obj TagTypSearchReq
	obj.PageNum = 1
	obj.PageSize = 20
	libUtils.ForEachStruct(obj, libUtils.FieldInfoCallBack)

	v, ok := libUtils.GetStructItemValue(obj, "PageNum")
	if ok {
		num := v.(int)

		fmt.Printf("num:%d", num)
	}
	fmt.Println()

	v, ok = libUtils.GetStructItemValue(obj, "PageSize")
	if ok {
		num := v.(int)

		fmt.Printf("size:%d", num)
	}
	fmt.Println()

}
