/*
 * @Descripttion: 字段默认值填充函数
 * @version:
 * @Author: lfzxs@qq.com
 * @Date: 2023-11-15 14:01:17
 * @LastEditors: lfzxs@qq.com
 * @LastEditTime: 2023-12-01 17:11:34
 */
package libService

import (
	"gitee.com/g_boot/chkboot-common/library/libUtils"
	"github.com/gogf/gf/v2/util/guid"
	"github.com/google/uuid"
)

type FildFillFunc func() string

var FunMap = map[string]FildFillFunc{
	"uuid":        Uuid,
	"snowflakeId": SnowflakeId,
	"uid":         Uid,
}

func Uuid() string {
	return uuid.NewString()
}

func SnowflakeId() string {
	return libUtils.SnowflakeIdStr()
}

func Uid() string {
	return guid.S()
}
