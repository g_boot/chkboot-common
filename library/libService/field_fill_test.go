package libService_test

import (
	"fmt"
	"testing"

	"gitee.com/g_boot/chkboot-common/library/libService"
)

func TestId(t *testing.T) {
	fmt.Printf("UUID:%s \n",libService.Uuid())
	fmt.Printf("SnowflakeId:%s \n",libService.SnowflakeId())
	fmt.Printf("Uid:%s \n",libService.Uid())
}


func TestGetFun(t *testing.T) {
	fmt.Printf("UUID:%s \n",libService.FunMap["uuid"]())
	fmt.Printf("SnowflakeId:%s \n",libService.FunMap["snowflakeId"]())
	fmt.Printf("Uid:%s \n",libService.FunMap["uid"]())
	funobj := libService.FunMap["uid1"]
	if(nil != funobj){
		fmt.Printf("Uid:%s \n",funobj())
	}
	
}